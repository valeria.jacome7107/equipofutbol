<?php
  class Jugador extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("jugador",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $jugadores=$this->db->get("jugador");
      if ($jugadores->num_rows()>0) {
        return $jugadores->result();
      } else {
        return false;
      }
    }

    public function consultarTodosConPosicionEquipo() {
    $this->db->select('jugador.*, posicion.nombre_pos AS nombre_pos, equipo.nombre_equi AS nombre_equi');
    $this->db->from('jugador');
    $this->db->join('posicion', 'jugador.fk_id_pos = posicion.id_pos', 'left');
    $this->db->join('equipo', 'jugador.fk_id_equi = equipo.id_equi', 'left');
    $query = $this->db->get();
    return $query->result();
}


    // Obtener hospital por ID
function obtenerPorId($id_jug)
{
    $this->db->where("id_jug", $id_jug);
    $jugador = $this->db->get("jugador");
    if ($jugador->num_rows() > 0) {
        return $jugador->row();
    } else {
        return false;
    }
}



    //eliminacion de hospital por id
    function eliminar($id_jug){
        $this->db->where("id_jug",$id_jug);
        return $this->db->delete("jugador");
    }

    //funcion para actualizar hospitales
function actualizar($id_jug,$datos){
  $this->db->where("id_jug",$id_jug);
  return $this->db->update("jugador",$datos);
}




function obtenerListadoPosiciones()
{
    $posiciones = $this->db->get("posicion")->result();
    return $posiciones;
}


   function obtenerListadoEquipos()
   {
       $equipos = $this->db->get("equipo")->result();
       return $equipos;
   }



  }//Fin de la clase



?>
