<?php
    class Equipo extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      // Utiliza el método set() para establecer los datos que deseas insertar
      $this->db->set($datos);

      // Luego, realiza la inserción en la tabla 'editorial'
      $respuesta = $this->db->insert("equipo");

      return $respuesta;
  }


    //Consulta de datos
    function consultarTodos(){
      $equipos=$this->db->get("equipo");
      if ($equipos->num_rows()>0) {
        return $equipos->result();
      } else {
        return false;
      }
    }


function obtenerPorId($id_equi)
{function insertar($datos){
      // Utiliza el método set() para establecer los datos que deseas insertar
      $this->db->set($datos);

      // Luego, realiza la inserción en la tabla 'editorial'
      $respuesta = $this->db->insert("equipo");

      return $respuesta;
  }
    $this->db->where("id_equi", $id_equi);
    $equipo = $this->db->get("equipo");
    if ($equipo->num_rows() > 0) {
        return $equipo->row();
    } else {
        return false;
    }
}



    function eliminar($id_equi){
        $this->db->where("id_equi",$id_equi);
        return $this->db->delete("equipo");
    }

function actualizar($id_equi,$datos){
  $this->db->where("id_equi",$id_equi);
  return $this->db->update("equipo",$datos);
}


  }//Fin de la clase
?>
