<?php
    class Posicion extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      // Utiliza el método set() para establecer los datos que deseas insertar
      $this->db->set($datos);

      // Luego, realiza la inserción en la tabla 'editorial'
      $respuesta = $this->db->insert("posicion");

      return $respuesta;
  }


    //Consulta de datos
    function consultarTodos(){
      $posiciones=$this->db->get("posicion");
      if ($posiciones->num_rows()>0) {
        return $posiciones->result();
      } else {
        return false;
      }
    }


function obtenerPorId($id_pos)
{function insertar($datos){
      // Utiliza el método set() para establecer los datos que deseas insertar
      $this->db->set($datos);

      // Luego, realiza la inserción en la tabla 'editorial'
      $respuesta = $this->db->insert("posicion");

      return $respuesta;
  }
    $this->db->where("id_pos", $id_pos);
    $posicion = $this->db->get("posicion");
    if ($posicion->num_rows() > 0) {
        return $posicion->row();
    } else {
        return false;
    }
}



    function eliminar($id_pos){
        $this->db->where("id_pos",$id_pos);
        return $this->db->delete("posicion");
    }

function actualizar($id_pos,$datos){
  $this->db->where("id_pos",$id_pos);
  return $this->db->update("posicion",$datos);
}


  }//Fin de la clase
?>
