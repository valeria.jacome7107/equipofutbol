<?php

/**
 *
 */
class Posiciones extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model("Posicion");
    //deshabilitando errores y advertencias de PHP
    error_reporting(0);

  }
  public function index(){

    $data["listadoPosiciones"]=
                    $this->Posicion->consultarTodos();
    $this->load->view("header");
    $this->load->view("posiciones/index",$data);
    $this->load->view("footer");
  }
  public function borrar($id_pos){
    $this->Posicion->eliminar($id_pos);
    $this->session->set_flashdata("confirmacion", "Posición eliminado existosamente");
    redirect('posiciones/index');
}



  //Renderizando hospitales
  public function nuevo(){
      $this->load->view("header");
      $this->load->view("posiciones/index", $data);
      $this->load->view("footer");
  }

  //Renderizar el formulario de edicion
  public function editar($id_pos){
      $this->load->model("Posicion");
      $data["posicionEditar"] = $this->Posicion->obtenerPorId($id_pos);
      $this->load->view("header");
      $this->load->view("posiciones/editar", $data);
      $this->load->view("footer");
  }



  //insertar hospitales
  public function guardarPosicion(){

    $datosNuevosPosicion=array(
      "id_pos" => $this->input->post("id_pos"),
      "nombre_pos" => $this->input->post("nombre_pos"),
      "descripcion_pos" => $this->input->post("descripcion_pos"),

    );
    $this->Posicion->insertar($datosNuevosPosicion);
      //flash crear una sesion tipo flash
      $this->session->set_flashdata("confirmacion","Posición guardado exitosamente");
      redirect('posiciones/index');
  }

  public function actualizarPosicion()
{
 $id_pos = $this->input->post("id_pos");

 // Obtener los datos actuales de la agencia
 $posicionEditar = $this->Posicion->obtenerPorId($id_pos);

 // Obtener los datos actualizados del formulario
 $datosPosicion = array(
      "id_pos" => $this->input->post("id_pos"),
      "nombre_pos" => $this->input->post("nombre_pos"),
      "descripcion_pos" => $this->input->post("descripcion_pos"),
 );



 // Actualizar la agencia con los nuevos datos
 $this->Posicion->actualizar($id_pos, $datosPosicion);

 // Flash message
 $this->session->set_flashdata("confirmacion", "Posicion actualizado exitosamente");

 // Redireccionar a la página de lista de agencias
 redirect('posiciones/index');
}





}



 ?>
