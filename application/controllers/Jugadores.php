<?php

/**
 *
 */
class Jugadores extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model("Jugador");
    error_reporting(0);

  }
  public function index(){
    // Utiliza una sola función de consulta que traiga la información completa
        $data["listadoJugadores"] = $this->Jugador->consultarTodosConPosicionEquipo();
        $data["listadoPosiciones"] = $this->Jugador->obtenerListadoPosiciones();
        $data["listadoEquipos"] = $this->Jugador->obtenerListadoEquipos();

        $this->load->view("header");
        $this->load->view("jugadores/index", $data);
        $this->load->view("footer");
    }

  public function borrar($id_jug){
    $this->Jugador->eliminar($id_jug);
    $this->session->set_flashdata("confirmacion", "Jugadores eliminada existosamente");
    redirect('jugadores/index');
}

public function guardarJugadores(){
    $datosNuevosJugador = array(
      "id_jug" => $this->input->post("id_jug"),
      "apellido_jug" => $this->input->post("apellido_jug"),
      "nombre_jug" => $this->input->post("nombre_jug"),
      "estatura_jug" => $this->input->post("estatura_jug"),
      "salario_jug" => $this->input->post("salario_jug"),
      "estado_jug" => $this->input->post("estado_jug"),
      "fk_id_pos" => $this->input->post("fk_id_pos"),
      "fk_id_equi" => $this->input->post("fk_id_equi")

    );

    $this->Jugador->insertar($datosNuevosJugador);

    // Flash message
    $this->session->set_flashdata("confirmacion","Devolución guardado exitosamente");

    redirect('jugadores/index');
}



  public function nuevo(){
      $this->load->view("header");
      $this->load->view("jugadores/nuevo");
      $this->load->view("footer");
  }

  //Renderizar el formulario de edicion
  public function editar($id_jug){

      $data["listadoPosiciones"] = $this->Jugador->obtenerListadoPosiciones();
      $data["listadoEquipos"] = $this->Jugador->obtenerListadoEquipos();
      $data["jugadorEditar"] = $this->Jugador->obtenerPorId($id_jug);

      $this->load->view("header");
      $this->load->view("jugadores/editar", $data);
      $this->load->view("footer");
  }

#aqui me quede ----------------------------------------------------------

public function actualizarJugador(){
    $id_jug = $this->input->post("id_jug");

    // Obtener los datos actuales de la autoría
    $jugadorEditar = $this->Jugador->obtenerPorId($id_jug);

    // Obtener los datos actualizados del formulario
    $datosJugador = array(
      "id_jug" => $this->input->post("id_jug"),
      "apellido_jug" => $this->input->post("apellido_jug"),
      "nombre_jug" => $this->input->post("nombre_jug"),
      "estatura_jug" => $this->input->post("estatura_jug"),
      "salario_jug" => $this->input->post("salario_jug"),
      "estado_jug" => $this->input->post("estado_jug"),
      "fk_id_pos" => $this->input->post("fk_id_pos"),
      "fk_id_equi" => $this->input->post("fk_id_equi")
    );

    // Actualizar los datos de la autoría en la base de datos
    $this->Jugador->actualizar($id_jug, $datosJugador);

    // Flash message
    $this->session->set_flashdata("confirmacion", "Jugador actualizada exitosamente");

    // Redireccionar a la página de lista de revistas
    redirect('jugadores/index');
}


}


 ?>
