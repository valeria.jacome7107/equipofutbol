<div style="padding: 150px 70px 20px 100px">

    <h1>
        <b>
            <i class="fa-solid fa-pen-to-square"></i>
            EDITAR EQUIPO
        </b>
    </h1>
    <br>

    <form class="text-dark" action="<?php echo site_url('equipos/actualizarEquipo'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_personal">

        <input type="hidden" value="<?php echo $equipoEditar->id_equi; ?>" name="id_equi" id="id_equi">
        <div class="mb-3 text-dark">
            <label for="nombre_equi" class="form-label text-dark"><b>Nombre del Equipo:</b></label>
            <input id="nombre_equi" type="text" name="nombre_equi" value="<?php echo $equipoEditar->nombre_equi; ?>" oninput="" placeholder="Ingrese el nombre del equipo" class="form-control" required>
        </div>
        <div class="mb-3 text-dark">
            <label for="siglas_equi" class="form-label text-dark"><b>Siglas del Equipo:</b></label>
            <input id="siglas_equi" type="text" name="siglas_equi" value="<?php echo $equipoEditar->siglas_equi; ?>" placeholder="Ingrese las siglas del equipo" class="form-control" required>
        </div>
        <div class="mb-3 text-dark">
            <label for="fundacion_equi" class="form-label text-dark"><b>Fundación del Equipo:</b></label>
            <input id="fundacion_equi" type="text" name="fundacion_equi" value="<?php echo $equipoEditar->fundacion_equi; ?>" placeholder="Ingrese la fundación del equipo" class="form-control" required>
        </div>
        <div class="mb-3">
             <label for="region_equi" class="form-label"><b>Región:</b></label>
             <select id="region_equi" name="region_equi" class="form-control" required>
                 <option value="">Seleccione la región</option>
                 <option value="Costa" <?php if ($equipoEditar->region_equi == "Costa") echo "selected"; ?>>Costa</option>
                 <option value="Sierra" <?php if ($equipoEditar->region_equi == "Sierra") echo "selected"; ?>>Sierra</option>
                 <option value="Amazonia" <?php if ($equipoEditar->region_equi == "Amazonia") echo "selected"; ?>>Amazonia</option>
                 <option value="Galápagos" <?php if ($equipoEditar->region_equi == "Galápagos") echo "selected"; ?>>Galápagos</option>
             </select>
         </div>
        <div class="mb-3">
            <label for="numero_titulos_equi" class="form-label"><b>Numero de títulos:</b></label>
            <input id="numero_titulos_equi" type="text" name="numero_titulos_equi" value="<?php echo $equipoEditar->numero_titulos_equi; ?>" placeholder="Ingrese el número de títulos" class="form-control" required>
        </div>





        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
                &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href=" <?php echo site_url('equipos/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
            </div>

        </div>

    </form>

</div>

<script type="text/javascript">
function validarLetras(input) {
  input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
  

}


function validarNumeros(input) {
input.value = input.value.replace(/\D/g, '');
}

</script>



<script type="text/javascript">
$(document).ready(function() {
  $.validator.addMethod("fechaActual", function(value, element) {
    var fechaActual = new Date();
    var fechaInscripcion = new Date(value);
    return fechaInscripcion <= fechaActual;
  }, "La fecha de contratación no puede ser después de la fecha actual.");
    $("#frm_nuevo_personal").validate({
        rules: {
          "nombre": {
              required: true,
              minlength: 3,
              maxlength: 50

          },
          "dirección": {
              required: true,
              minlength: 3,
              maxlength: 150

          },
            "puesto": {
                required: true,
                minlength: 3,
                maxlength: 50

            },

            "telefono": {
              required: true,
              minlength: 7,
              maxlength: 10
            },
            "fecha_contratacion": {
              required: true,
              fechaActual: true

            },

            "estado": {
              required: true
            },
            "fotografia": {
              required: true
            }

        },
        messages: {
          "nombre": {
              required: "Ingrese el nombre del empleado",
              minlength: "El nombre del empleado no debe tener menos de 3 caracteres",
              maxlength: "El nombre del empleado no debe tener más de 50 caracteres"

          },
          "direccion": {
              required: "Ingrese la direccion",
              minlength: "La dirección no debe tener menos de 3 caracteres",
              maxlength: "La dirección no debe tener más de 150 caracteres"

          },
            "puesto": {
                required: "Ingrese el puesto",
                minlength: "El puesto no debe tener menos de 3 caracteres",
                maxlength: "El puesto no debe tener más de 50 caracteres"

            },
            "telefono": {
              required: "Debe ingresar el telefono",
              minlength: "El telefono no debe tener menos de 7 numeros",
              maxlength: "El teléfono no debe tener más de 10 numeros"


            },
            "fecha_contratacion": {
              required: "Debe ingresar la fecha de contración"
            },

            "estado": {
              required: "Debe escoger el estado del empleado"
            },

            "fotografia": {
              required: "Debe ingresar la foto del empleado"
            }


        }
    });
});


</script>



<style media="screen">
  input{
    color: black !important;
  }
</style>
