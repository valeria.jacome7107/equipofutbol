<div style="padding: 150px 70px 20px 100px">
    <div class="text-center">
        <h1><i class="fa-solid fa-book"></i>&nbsp;&nbsp;EQUIPO</h1>
    </div>
    <div class="row">
        <div class="col-md-12 text-end">
            <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                <i class="fa fa-plus-circle fa-1x"></i> Agregar nuevo Equipo
            </button>
        </div>
    </div>
    <br>

    <?php if ($listadoEquipos): ?>
    <table class="table table-striped text-center">
        <thead class="table-dark">
            <tr>
                <th>ID</th>
                <th>NOMBRE DEL EQUIPO</th>
                <th>SIGLAS DEL EQUIPO</th>
                <th>FUNDACIÓN DEL EQUIPO</th>
                <th>REGIÓN</th>
                <th>NUMERO DE TITULOS</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoEquipos as $equipo): ?>
            <tr>
                <td class="text-dark"><?php echo $equipo->id_equi; ?></td>
                <td class="text-dark"><?php echo $equipo->nombre_equi; ?></td>
                <td class="text-dark"><?php echo $equipo->siglas_equi; ?></td>
                <td class="text-dark"><?php echo $equipo->fundacion_equi; ?></td>
                <td class="text-dark"><?php echo $equipo->region_equi; ?></td>
                <td class="text-dark"><?php echo $equipo->numero_titulos_equi; ?></td>
                <td>
                    <a href="<?php echo site_url('equipos/editar/').$equipo->id_equi; ?>" class="btn btn-warning" title="Editar">
                        <i class="fa fa-pen"></i>
                    </a>
                    <a href="#" class="btn btn-danger" onclick="eliminarRegistro('<?php echo site_url('equipos/borrar/').$equipo->id_equi; ?>')">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div class="modal-footer"></div>

    <?php else: ?>
    <div class="alert alert-danger">
        No se encontró equipo registrado
    </div>
    <?php endif; ?>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nuevo Personal de Equipo</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form class="text-dark" action="<?php echo site_url('equipos/guardarEquipo') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_personal">
                      
                        <div class="mb-3 text-dark">
                            <label for="nombre_equi" class="form-label text-dark"><b>Nombre del Equipo:</b></label>
                            <input id="nombre_equi" type="text" name="nombre_equi" value="" oninput="validarLetras(this)" placeholder="Ingrese el nombre del equipo" class="form-control" required>
                        </div>
                        <div class="mb-3 text-dark">
                            <label for="siglas_equi" class="form-label text-dark"><b>Siglas del Equipo:</b></label>
                            <input id="siglas_equi" type="text" name="siglas_equi" value="" placeholder="Ingrese las siglas del equipo" class="form-control" required>
                        </div>
                        <div class="mb-3 text-dark">
                            <label for="fundacion_equi" class="form-label text-dark"><b>Fundación del Equipo:</b></label>
                            <input id="fundacion_equi" type="text" name="fundacion_equi" value="" placeholder="Ingrese la fundación del equipo" class="form-control" required>
                        </div>
                    <div class="mb-3">
                    <label for="region_equi" class="form-label"><b>Región:</b></label>
                    <select id="region_equi" name="region_equi" class="form-control" required>
                        <option value="">Seleccione la Region</option>
                        <option value="Costa">Costa</option>
                        <option value="Sierra">Sierra</option>
                        <option value="Amazonia">Amazonia</option>
                        <option value="Galápagos">Galápagos</option>
                    </select>
                        <div class="mb-3">
                            <label for="numero_titulos_equi" class="form-label"><b>Numero de títulos:</b></label>
                            <input id="numero_titulos_equi" type="text" name="numero_titulos_equi" value="" placeholder="Ingrese el número de títulos" class="form-control" required>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-auto">
                                <button type="submit" name="button" class="btn btn-success">
                                    <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                                </button>
                            </div>
                            <div class="col-auto">
                                <a class="btn btn-danger" href="<?php echo site_url('equipos/index') ?>">
                                    <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function eliminarRegistro(url) {
    Swal.fire({
        title: '¿Estás seguro de eliminar este registro?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, elimínalo!',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            // Si el usuario confirma la eliminación, redireccionamos a la URL especificada
            window.location.href = url;
        } else {
            // Si el usuario cancela, mostramos un mensaje de cancelación
            Swal.fire(
                'Cancelado',
                'Tu registro no ha sido eliminado :P',
                'error'
            );
        }
    });
}

function validarLetras(input) {
    input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
 
}

function validarNumeros(input) {
    input.value = input.value.replace(/\D/g, '');
}
</script>

<style media="screen">
    input {
        color: black !important;
    }
</style>
