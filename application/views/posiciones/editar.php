<div style="padding: 150px 70px 20px 100px">

    <h1>
        <b>
            <i class="fa-solid fa-pen-to-square"></i>
            EDITAR EQUIPO
        </b>
    </h1>
    <br>

    <form class="text-dark" action="<?php echo site_url('posiciones/actualizarPosicion'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_personal">

        <input type="hidden" value="<?php echo $posicionEditar->id_pos; ?>" name="id_pos" id="id_pos">
        <div class="mb-3 text-dark">
            <label for="nombre_pos" class="form-label text-dark"><b>Nombre de la posición:</b></label>
             <input id="nombre_pos" type="text" name="nombre_pos" value="<?php echo $posicionEditar->nombre_pos; ?>" oninput="validarLetras(this)" placeholder="Ingrese el nombre de la posición " class="form-control" required>
        </div>
        <div class="mb-3 text-dark">
            <label for="descripcion_pos" class="form-label text-dark"><b>Descripción de la posición:</b></label>
            <input id="descripcion_pos" type="text" name="descripcion_pos" value="<?php echo $posicionEditar->descripcion_pos; ?>" placeholder="Ingrese la posición" class="form-control" required>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
                &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href=" <?php echo site_url('posiciones/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
            </div>

        </div>

    </form>

</div>

<script type="text/javascript">
function validarLetras(input) {
  input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');

}


function validarNumeros(input) {
input.value = input.value.replace(/\D/g, '');
}

</script>
<script>
  $(document).ready(function () {
    $("#fotografia_nueva").fileinput({
      //showUpload:false
      //showRemove: false,
      language:'es',
    });
  });
</script>


<script type="text/javascript">
$(document).ready(function() {
  $.validator.addMethod("fechaActual", function(value, element) {
    var fechaActual = new Date();
    var fechaInscripcion = new Date(value);
    return fechaInscripcion <= fechaActual;
  }, "La fecha de contratación no puede ser después de la fecha actual.");
    $("#frm_nuevo_personal").validate({
        rules: {
          "nombre": {
              required: true,
              minlength: 3,
              maxlength: 50

          },
          "dirección": {
              required: true,
              minlength: 3,
              maxlength: 150

          },
            "puesto": {
                required: true,
                minlength: 3,
                maxlength: 50

            },

            "telefono": {
              required: true,
              minlength: 7,
              maxlength: 10
            },
            "fecha_contratacion": {
              required: true,
              fechaActual: true

            },

            "estado": {
              required: true
            },
            "fotografia": {
              required: true
            }

        },
        messages: {
          "nombre": {
              required: "Ingrese el nombre del empleado",
              minlength: "El nombre del empleado no debe tener menos de 3 caracteres",
              maxlength: "El nombre del empleado no debe tener más de 50 caracteres"

          },
          "direccion": {
              required: "Ingrese la direccion",
              minlength: "La dirección no debe tener menos de 3 caracteres",
              maxlength: "La dirección no debe tener más de 150 caracteres"

          },
            "puesto": {
                required: "Ingrese el puesto",
                minlength: "El puesto no debe tener menos de 3 caracteres",
                maxlength: "El puesto no debe tener más de 50 caracteres"

            },
            "telefono": {
              required: "Debe ingresar el telefono",
              minlength: "El telefono no debe tener menos de 7 numeros",
              maxlength: "El teléfono no debe tener más de 10 numeros"


            },
            "fecha_contratacion": {
              required: "Debe ingresar la fecha de contración"
            },

            "estado": {
              required: "Debe escoger el estado del empleado"
            },

            "fotografia": {
              required: "Debe ingresar la foto del empleado"
            }


        }
    });
});


</script>



<style media="screen">
  input{
    color: black !important;
  }
</style>
