
<div style="padding: 150px 70px 20px 100px">
     <div class="text-center">
       <h1><i class="fa-solid fa-book-open"></i>&nbsp;&nbsp;JUGADORES</h1>
    </div>
    <div class="row">
    <div class="col-md-12 text-end">

      <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
         <i class="fa fa-plus-circle fa-1x"></i> Agregar Nuevo Jugador
      </button>


    </div>

  </div><br>


  <?php if ($listadoJugadores): ?>
    <table class="table table-striped text-center">
    <thead class="table-dark">
        <tr>
            <th>ID</th>
            <th>APELLIDO DEL JUGADOR</th>
            <th>NOMBRE DEL JUGADOR</th>
            <th>ESTATURA</th>
            <th>SALARIO</th>
            <th>ESTADO</th>
            <th>POSICIÓN</th>
            <th>EQUIPO</th>
            <th>ACCIONES</th>

        </tr>
    </thead>
    <tbody>
        <?php foreach ($listadoJugadores as $jugador): ?>
            <tr>
                <td class="text-dark"><?php echo $jugador->id_jug; ?></td>
                <td class="text-dark"><?php echo $jugador->apellido_jug; ?></td>
                <td class="text-dark"><?php echo $jugador->nombre_jug; ?></td>
                <td class="text-dark"><?php echo $jugador->estatura_jug; ?></td>
                <td class="text-dark"><?php echo $jugador->salario_jug; ?></td>
                <td class="text-dark"><?php echo $jugador->estado_jug; ?></td>
                <td class="text-dark"><?php echo $jugador->nombre_pos; ?></td>
                <td class="text-dark"><?php echo $jugador->nombre_equi; ?></td>

                <td>
                    <a href="<?php echo site_url('jugadores/editar/').$jugador->id_jug; ?>" class="btn btn-warning" title="Editar">
                        <i class="fa fa-pen"></i>
                    </a>
                    <a href="#" class="btn btn-danger" onclick="eliminarRegistro('<?php echo site_url('jugadores/borrar/').$jugador->id_jug; ?>')">
                        <i class="fa fa-trash"></i>
                    </a>



                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>



  <div class="modal-footer">

  </div>

              </div>
            </div>
          </div>
    <?php else: ?>
          <div class="alert alert-danger">
              No se encontro jugadores registrados
          </div>
  <?php endif; ?>
</div>

<!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"> Nuevo Jugador
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
<div class="container">
    <form  class="text-dark"action="<?php echo site_url('jugadores/guardarJugadores') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_devolucion">

      <div class="mb-3 text-dark">
    <label for=""><b>Posición:</b></label>
    <select name="fk_id_pos" id="fk_id_pos" class="form-control" required>
        <option value="">--Seleccione la posición--</option>
        <?php foreach ($listadoPosiciones as $posicion): ?>
            <option value="<?php echo $posicion->id_pos; ?>"><?php echo $posicion->nombre_pos; ?></option>
        <?php endforeach; ?>
    </select>
      </div>


        <div class="mb-3 text-dark">
      <label for="fk_id_equi"><b> Equipo:</b></label>
      <select name="fk_id_equi" id="fk_id_equi" class="form-control" required>
          <option value="">--Seleccione el equipo--</option>
          <?php foreach ($listadoEquipos as $equipo): ?>
              <option value="<?php echo $equipo->id_equi; ?>"><?php echo $equipo->nombre_equi; ?> </option>
          <?php endforeach; ?>
      </select>
        </div>

        <div class="mb-3">
            <label for="apellido_jug" class="form-label"><b>Apellido del Jugador:</b></label>
            <input id="apellido_jug" type="text" name="apellido_jug" value="" oninput="" placeholder="Ingrese el apellido del jugador" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="nombre_jug" class="form-label"><b>Nombre del Jugador:</b></label>
            <input id="nombre_jug" type="text" name="nombre_jug" value="" oninput="" placeholder="Ingrese el nombre del jugador" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="estatura_jug" class="form-label"><b>Estatura del Jugador:</b></label>
            <input id="estatura_jug" type="text" name="estatura_jug" value="" oninput="" placeholder="Ingrese la estatura del jugador" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="salario_jug" class="form-label"><b>Salario del Jugador:</b></label>
            <input id="salario_jug" type="text" name="salario_jug" value="" oninput="" placeholder="Ingrese el salario del jugador" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="estado_jug" class="form-label"><b>Estado:</b></label>
            <select id="estado_jug" name="estado_jug" class="form-control" required>
                <option value="">Seleccione el estado</option>
                <option value="Activo">Activo</option>
                <option value="Inactivo">Inactivo</option>
            </select>
        </div>


        <div class="row justify-content-end">
            <div class="col-auto">
                <button type="submit" name="button" class="btn btn-success">
                    <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                </button>
            </div>
            <div class="col-auto">
                <a class="btn btn-danger" href="<?php echo site_url('jugadores/index') ?>">
                    <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                </a>
            </div>
        </div>
    </form>
</div>
</div>

</div>

</div>
</div>
<script>
function eliminarRegistro(url) {
              Swal.fire({
                  title: '¿Estas seguro de eliminar este registro?',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: '¡Sí, eliminalo!',
                  cancelButtonText: 'Cancelar'
              }).then((result) => {
                  if (result.isConfirmed) {
                      // Si el usuario confirma la eliminación, redireccionamos a la URL especificada
                      window.location.href = url;
                  } else {
                      // Si el usuario cancela, mostramos un mensaje de cancelación
                      Swal.fire(
                          'Cancelado',
                          'Tu registro no ha sido eliminado :P',
                          'error'
                      );
                  }
              });
          }
      </script>

      <script type="text/javascript">
      function validarLetras(input) {
        input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
     

      }


      function validarNumeros(input) {
      input.value = input.value.replace(/\D/g, '');
      }

      </script>

      <script type="text/javascript">
      $.validator.addMethod("fechaNoPosterior", function(value, element) {
          var fecha_actual = new Date();
          var fecha_devolucion = new Date(value);
          return fecha_devolucion <= fecha_actual;
          }, "La fecha de devolución no puede ser posterior a la fecha actual.");

          $("#frm_nuevo_devolucion").validate({
          rules: {
           "fkid_personal": {
               required: true
           },
           "fkid_prestamo": {
               required: true
           },
           "fecha_devolucion": {
               required: true,
               fechaNoPosterior: true
           },
           "estado": {
               required: true
           }
          },
          messages: {
           "fkid_personal": {
               required: "Escoja el personal"
           },
           "fkid_prestamo": {
               required: "Seleccione la fecha de préstamo"
           },
           "fecha_devolucion": {
               required: "Ingrese la fecha de devolución",
               fechaNoPosterior: "La fecha de devolución no puede ser posterior a la fecha actual"
           },
           "estado": {
               required: "Escoja el estado de la devolución"
           }
          }
          });



      </script>
      <style media="screen">
        input{
          color: black !important;
        }
      </style>
