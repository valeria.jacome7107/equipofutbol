<div style="padding: 150px 70px 20px 100px">

    <h1>
        <b>
            <i class="fa-solid fa-pen-to-square"></i>
            EDITAR JUGADOR
        </b>
    </h1>
    <br>

    <form class="text-dark" action="<?php echo site_url('jugadores/actualizarJugador'); ?>" method="post" enctype="multipart/form-data" id="">

        <input type="hidden" value="<?php echo $jugadorEditar->id_jug; ?>" name="id_jug" id="id_jug">

<div class="mb-3 text-dark">
    <label for=""><b>Posición:</b></label>
    <select name="fk_id_pos" id="fk_id_pos" class="form-control selectpicker" required>
        <option value="">--Seleccione la posición--</option>
        <?php foreach ($listadoPosiciones as $posicion): ?>
            <option value="<?php echo $posicion->id_pos; ?>" <?php if ($posicion->id_pos == $jugadorEditar->fk_id_pos) echo "selected"; ?>>
           <?php echo $posicion->nombre_pos; ?>
       </option>
        <?php endforeach; ?>
    </select>
      </div>

      <div class="mb-3 text-dark">
      <label for="fk_id_equi"><b> Equipo:</b></label>
      <select name="fk_id_equi" id="fk_id_equi" class="form-control" required>
          <option value="">--Seleccione el equipo--</option>
          <?php foreach ($listadoEquipos as $equipo): ?>
            <option value="<?php echo $equipo->id_equi; ?>" <?php if ($equipo->id_equi == $jugadorEditar->fk_id_equi) echo "selected"; ?>>
            <?php echo $equipo->nombre_equi; ?>
          <?php endforeach; ?>
      </select>
        </div>

    <div class="mb-3">
            <label for="apellido_jug" class="form-label"><b>Apellido del Jugador:</b></label>
            <input id="apellido_jug" type="text" name="apellido_jug" value="<?php echo $jugadorEditar->apellido_jug; ?>" oninput="" placeholder="Ingrese el apellido del jugador" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="nombre_jug" class="form-label"><b>Nombre del Jugador:</b></label>
            <input id="nombre_jug" type="text" name="nombre_jug" value="<?php echo $jugadorEditar->nombre_jug; ?>" oninput="" placeholder="Ingrese el nombre del jugador" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="estatura_jug" class="form-label"><b>Estatura del Jugador:</b></label>
            <input id="estatura_jug" type="text" name="estatura_jug" value="<?php echo $jugadorEditar->estatura_jug; ?>" oninput="" placeholder="Ingrese la estatura del jugador" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="salario_jug" class="form-label"><b>Salario del Jugador:</b></label>
            <input id="salario_jug" type="text" name="salario_jug" value="<?php echo $jugadorEditar->salario_jug; ?>" oninput="" placeholder="Ingrese el salario del jugador" class="form-control" required>
        </div>
  <div class="mb-3">
     <label for="estado_jug" class="form-label"><b>Estado:</b></label>
     <select id="estado_jug" name="estado_jug" class="form-control" required>
         <option value="">Seleccione el estado</option>
         <option value="Activo" <?php if ($jugadorEditar->estado_jug == "Activo") echo "selected"; ?>>Activo</option>
         <option value="Inactivo" <?php if ($jugadorEditar->estado_jug == "Inactivo") echo "selected"; ?>>Inactivo</option>
     </select>
 </div>


        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
                &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href=" <?php echo site_url('jugadores/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
            </div>

        </div>

    </form>

</div>


<script type="text/javascript">
      function validarLetras(input) {
        input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
      

      }


      function validarNumeros(input) {
      input.value = input.value.replace(/\D/g, '');
      }

      </script>


      <script type="text/javascript">
          $(document).ready(function() {
          $("#frm_nuevo_devolucion").validate({
              rules: {
                  "fkid_personal": {
                      required: true
                  },
                  "fkid_prestamo": {
                      required: true
                  },
                  "fecha_devolucion": {
                      required: true,
                      fechaActual: true // Nueva regla personalizada
                  },
                  "estado": {
                      required: true
                  }
              },
              messages: {
                  "fkid_personal": {
                      required: "Escoja el personal"
                  },
                  "fkid_prestamo": {
                      required: "Seleccione la fecha de préstamo"
                  },
                  "fecha_devolucion": {
                      required: "Ingrese la fecha de devolución"
                  },
                  "estado": {
                      required: "Escoja el estado de la devolución"
                  }
              }
          });

          // Agregar regla personalizada para validar que la fecha de devolución no sea posterior a la fecha actual
          $.validator.addMethod("fechaActual", function(value, element) {
              var fecha_actual = new Date();
              var fecha_devolucion = new Date(value);
              return fecha_devolucion <= fecha_actual;
          }, "La fecha de devolución no puede ser posterior a la fecha actual.");
          });


      </script>
      <style media="screen">
        input{
          color: black !important;
        }
      </style>
